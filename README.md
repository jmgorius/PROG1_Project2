# [PROG1] Project 2

This repository contains the code for our second OCaml project as part of the
PROG1 course at ENS Rennes. The goal of the project is to implement the
Delaunay triangulation algorithm in OCaml in order to triangulate a set of
randomly generated points.

## Requirements

* Unix-based system
* OCaml 4.04.0 or higher
* OCaml libraries: `unix`, `graphics`

## Usage

The project in divided into two main parts, namely the test files and a main
program file. To launch the main file, simply type
```
sh run.sh main.ml
```
To launch a test file, type
```
sh runtest.sh <name-of-test-file>
```
where `<name-of-test-file>` is one of `tBorder.ml`, `tCircle.ml`, `tDet.ml`,
`tDraw.ml`, `tEdge.ml`, `tGui.ml`, `tPoint.ml`, `tTriangle.ml` and
`tTriangulation.ml`. As their name may suggest, the test files test each
module's functions in order to validate their behavior.

There were no added extensions.

## Get it!

You can get the latest version from the GitLab repository by using
```
git clone https://gitlab.com/jean-michel.gorius/PROG1_Project2.git
```
Note that you will need access rights to clone this repository. You can get
those from one of the authors listed below by sending an email containing
your GitLab username.

## License

This work is licensed under the terms of the Unlicense. You should have gotten
a copy of the license in the `LICENSE` file. If not, you can get a copy of it
from [the official website](http://unlicense.org/).

## Authors

* Jean-Michel GORIUS <jean-michel.gorius@ens-rennes.fr>
* Clément LEGRAND-DUCHESNE <clement.legrand-duchesne@ens-rennes.fr>
* Dylan MARINHO <dylan.marinho@ens-rennes.fr>
