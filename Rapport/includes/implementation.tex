\section{Implémentation}
  \subsection{Principe général}
  Afin d'obtenir une triangulation de Delaunay, on procède par
  incrémentation : on ajoute progressivement chacun des points à la
  triangulation. On part d'une triangulation initiale obtenu en
  divisant la fenêtre en deux triangles (Figure:
  \ref{fig:initialisation}).

  Pour chaque nouveau point $p$, on détermine alors
  l'ensemble des triangles l'ayant dans leurs cercles circonscrits,
  ceux ci doivent alors être supprimés (Figure: \ref{fig:etape1}).

  \begin{figure}
    \minipage{0.48\textwidth}
      \includegraphics[width=\linewidth]{images/init.png}
      \caption{Initialisation}\label{fig:initialisation}
    \endminipage\hfill
    \minipage{0.48\textwidth}
      \includegraphics[width=\linewidth]{images/etape1.png}
      \caption{Triangulation courante}\label{fig:etape1}
    \endminipage\hfill
  \end{figure}
    
  Vérifier l'appartenance d'un point au cercle circonscrit
  d'un triangle revient à vérifier la positivité d'un déterminant. En
  effet, un point $D$ est à l'intérieur du
  cercle circonscrit du triangle $(A,B,C)$ (où $A$, $B$, $C$ sont dans
  le sens direct) si et seulement si le déterminant

  $$
  \begin{vmatrix} 
    A_{x} & A_{y} & A_{x}^{2}+A_{y}^{2} & 1 \\
    B_{x} & B_{y} & B_{x}^{2}+B_{y}^{2} & 1 \\
    C_{x} & C_{y} & C_{x}^{2}+C_{y}^{2} & 1 \\
    D_{x} & D_{y} & D_{x}^{2}+D_{y}^{2} & 1
  \end{vmatrix} 
  $$
  est strictement positif.

  Il suffit donc de créer une fonction \texttt{in\_circle} déterminant
  si un point appartient au cercle circonscrit d'un triangle, et de
  l'appliquer à chaque triangle de la triangulation courante pour
  obtenir $\mathscr{T}$, l'ensemble des triangles à supprimer (Figure
  \ref{fig:etape2}).

  Considérons alors $\mathscr{S}$ l'ensemble de leurs sommets. Le
  polygone non croisé obtenu à partir de $\mathscr{S}$ délimite un
  convexe (auquel appartient le point $p$). On remarque de plus que
  les arêtes de la bordure n'appartiennent qu'à un unique triangle de
  $\mathscr{T}$ tandis que les autres font partie de deux triangles
  adjacents. Nous supprimons donc les arêtes en double, et ajoutons
  les nouveaux triangles ayant une des arêtes de la bordure pour base
  et le point $p$ pour troisième sommet (Figure \ref{fig:etape3}).

  \begin{figure}
    \minipage{0.48\textwidth}
      \includegraphics[width=\linewidth]{images/etape2.png}
      \caption{Triangles à modifier}\label{fig:etape2}
    \endminipage\hfill
    \minipage{0.48\textwidth}
      \includegraphics[width=\linewidth]{images/etape3.png}
      \caption{Nouvelle triangulation}\label{fig:etape3}
    \endminipage\hfill
  \end{figure}

  Afin de montrer les étapes successives de la triangulation, il
  suffit d'attendre une entrée de l'utilisateur entre chaque ajout de
  point.
  
  
  \subsection{Difficultés rencontrées}
    \subsubsection*{Typage}
    Nous avons créé des types abstraits pour représenter les points,
    les arêtes et les triangles. Il faut bien prendre garde lors de la
    construction des tests d'égalité des arêtes de ne pas prendre en
    compte l'ordre des sommets.
  
    Plutôt que d'utiliser des listes, nous avons choisi de représenter
    les groupes d'objets manipulés dans des ensembles. Cela présente
    plusieurs avantages: cela ne met pas en avant un ordre particuler
    sur les objets, et de plus, un élément peut apparaître plusieus fois
    dans une liste tandis qu'il sera unique dans un ensemble, ce qui
    permet de ne pas tracer les objets plusieurs fois à l'écran. Nous
    avons premièrement pensé définir un type abstrait pour chaque
    nouveau type d'éléments désiré, avant de rencontrer des
    difficultés lors de l'utilisation de fonctions telles que
    \texttt{map} ou \texttt{iter}. En effet, en appliquant une
    fonction sur un ensembe d'objets, l'ensemble d'objets obtenus ne
    sera pas forcement de même type. De plus, l'implémentation était
    assez lourde, et il semble plus naturel qu'un ensemble soit un
    terme général pouvant s'appliquer à plusieurs types d'éléments
    différents.
  
    Nous avons donc eu recours au foncteur \texttt{Set} de OCaml sur les types
    \texttt{point}, \texttt{edge} et \texttt{triangle}. Celui-ci
    nécessitant un ordre sur les éléments de l'ensemble, nous avons
    utilisé l'ordre lexicographique sur les coordonnées des points,
    puis pour les ensembles d'arêtes et de triangles, l'odre
    lexicographique sur les points. On notera que l'arête $(a,b)$ est
    à la fois désignée par $(a,b)$ et $(b,a)$. En revanche lors de la
    comparaison a une autre arête $e$, il est possible que $(a,b) < e$
    et $e < (a,b)$. Ceci reste de faible importance, car n'avons pas
    directement recours a l'ordre lexicograpique au cours de notre
    algorithme.

    L'utilisation du foncteur \texttt{Set} a pour avantage un gain de
    complexité lors de l'accès, l'ajout et la suppression à un
    élément. En revanche, cette implémentation trouvera ses limites,
    si l'on veut réutiliser le code écrit pour une triangularisation
    de Delaunay dynamique, dans laquelle on pourrait bouger un point
    du nuage à la souris. En effet, entre deux points très proches
    dans le plan peuvent s'intercaler un grand nombre de points du
    nuage (au sens de l'ordre lexicographique). Ainsi le déplacement
    d'un point du nuage pourra amener à faire un grand nombre de
    modifications sur la structure interne utilisée par Ocaml pour
    représenter un ensemble.

    Il est nécessaire d'utiliser des listes d'arêtes lors du calcul
    des nouveaux triangles à ajouter, afin de voir quelles arêtes sont
    présentes en double. De même, nous convertissons les ensembles en
    listes lorsque nous voulons effectuer un \texttt{iter} ou un
    \texttt{map}.

  \subsection{Vérification}
  Au terme de la triangulation, quel que soit le triangle $T$ et le
  point $p \in \mathscr{P}$, \texttt{in\_circle} appliquée à $T$ et
  $p$ renvoie \texttt{faux}, ce qui permet de vérifier le résultat
  expérimentalement.  Voici le résultat obtenu pour un nuage de 5
  points (Figure \ref{fig:cinqpts}) et un nuage de 1000 points (Figure
  \ref{fig:millepts})

  \begin{figure}
    \minipage{0.48\textwidth}
      \includegraphics[width=\linewidth]{images/Delaunay5.png}
      \caption{Triangulation d'un nuage aléatoire de 5 points}\label{fig:cinqpts}
    \endminipage\hfill
    \minipage{0.48\textwidth}
      \includegraphics[width=\linewidth]{images/Delaunay1000.png}
      \caption{Triangulation d'un nuage aléatoire de 1000 points}\label{fig:millepts}
    \endminipage\hfill
  \end{figure}
  