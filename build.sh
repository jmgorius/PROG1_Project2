#!/bin/sh

# Enable stop on error
set -e .

build_module () {
	echo -n "Building module [$1]..."
	ocamlc -I ../core -I ../../core -I ../mathsObjects -I ../maths/mathsObjects -I ../maths/delaunay -I ../../graphics -I ../graphics -c $1.mli
	ocamlc -I ../core -I ../../core -I ../mathsObjects -I ../maths/mathsObjects -I ../maths/delaunay -I ../../graphics -I ../graphics graphics.cma unix.cma -c $1.ml
	echo "done"
}

begin_unit () {
	echo "Entering directory '$1'"
	cd $1
}

end_unit () {
	echo "Leaving directory '$1'"
	cd ..
}

echo "Building: [PROG1] Project 2"

# Build the core modules
begin_unit core
build_module point
build_module triangle
build_module utility
end_unit core

# Build maths related module
begin_unit maths
begin_unit mathsObjects
build_module det
build_module circle
build_module edge
build_module randomPoints
end_unit mathsObjects
begin_unit delaunay
build_module border
build_module addPoint
end_unit delaunay
end_unit maths

# Build the graphics related modules
begin_unit graphics
build_module draw
build_module gui
end_unit graphics

# Build triangulation module
begin_unit maths
begin_unit delaunay
build_module triangulation
end_unit delaunay
end_unit maths

# Build tests
begin_unit tests
build_module tests
end_unit tests

# Done
echo "Build finished successfully"
