#!/bin/sh

find -type f -name "*.cmo" | xargs rm
find -type f -name "*.cmi" | xargs rm
find -type f -name "*.cmx" | xargs rm
find -type f -name "*.o" | xargs rm
