module Point =
	struct
		type t = {x: float; y: float}

    (** Lexicographic order on points *)
		let compare p1 p2 =
			let c = Pervasives.compare p1.x p2.x in
			if c = 0 then Pervasives.compare p1.y p2.y
			else c
		let make_point x y = {x = x; y = y}

		let get_x p = p.x
    let get_y p = p.y

    let is_equal p1 p2 = compare p1 p2 = 0
	end

module PointSet = Set.Make(Point)

type point = Point.t
type point_set = PointSet.t
