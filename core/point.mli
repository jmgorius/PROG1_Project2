(** File generated using `ocamlc -i point.ml > point.mli` *)

module Point :
	sig
		type t
		val compare : t -> t -> int
		val make_point: float -> float -> t
		val get_x: t -> float
    val get_y: t -> float
    val is_equal: t -> t -> bool
	end

module PointSet :
	sig
		type elt = Point.t
		type t = Set.Make(Point).t
		val empty : t
		val is_empty : t -> bool
		val mem : elt -> t -> bool
		val add : elt -> t -> t
		val singleton : elt -> t
		val remove : elt -> t -> t
		val union : t -> t -> t
		val inter : t -> t -> t
		val diff : t -> t -> t
		val compare : t -> t -> int
		val equal : t -> t -> bool
		val subset : t -> t -> bool
		val iter : (elt -> unit) -> t -> unit
		val map : (elt -> elt) -> t -> t
		val fold : (elt -> 'a -> 'a) -> t -> 'a -> 'a
		val for_all : (elt -> bool) -> t -> bool
		val exists : (elt -> bool) -> t -> bool
		val filter : (elt -> bool) -> t -> t
		val partition : (elt -> bool) -> t -> t * t
		val cardinal : t -> int
		val elements : t -> elt list
		val min_elt : t -> elt
		val min_elt_opt : t -> elt option
		val max_elt : t -> elt
		val max_elt_opt : t -> elt option
		val choose : t -> elt
		val choose_opt : t -> elt option
		val split : elt -> t -> t * bool * t
		val find : elt -> t -> elt
		val find_opt : elt -> t -> elt option
		val find_first : (elt -> bool) -> t -> elt
		val find_first_opt : (elt -> bool) -> t -> elt option
		val find_last : (elt -> bool) -> t -> elt
		val find_last_opt : (elt -> bool) -> t -> elt option
		val of_list : elt list -> t
	end

type point = Point.t
type point_set = PointSet.t
