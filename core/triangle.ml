module Triangle =
	struct
    type t = {p1: Point.point;
              p2: Point.point;
              p3: Point.point}

    (** Lexicographic order on triangles *)
    let compare t1 t2 =
      let c = Point.Point.compare t1.p1 t2.p1 in
      if c = 0 then
        let d = Point.Point.compare t1.p2 t2.p2 in
        if d = 0 then
          Point.Point.compare t1.p3 t2.p3
        else d
      else c

    let make_triangle p1 p2 p3 = {p1 = p1; p2 = p2; p3 = p3}

    let get_point t idx =
      if idx = 1 then t.p1
      else if idx = 2 then t.p2
      else t.p3
end

module TriangleSet = Set.Make(Triangle)

type triangle = Triangle.t
type triangle_set = TriangleSet.t
