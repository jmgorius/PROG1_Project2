(** Exchange arguments, useful for maps, folds, iters, etc. *)
let swap f a b = f b a
