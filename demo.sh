#!/bin/sh

clear

echo "-------------------- Start of demo session 2 --------------------"
echo "Welcome to the demo launcher!"
echo "Press [ENTER] to launch the build process"
read _

echo
sh build.sh
echo

echo "First test set: Core modules and basic functionalities"
echo "Press [ENTER] to launch the first unit tests"
read _

echo
sh runtest.sh tPoint.ml
read _
sh runtest.sh tTriangle.ml
read _
echo

echo "Second test set: Maths core modules"
echo "Press [ENTER] to launch the testing phase"
read _

echo
sh runtest.sh tEdge.ml
read _
sh runtest.sh tDet.ml
read _
sh runtest.sh tCircle.ml
read _
echo

echo "Third test set: Graphics related modules"
echo "Press [ENTER] to launch the test files"
read _

echo
sh runtest.sh tGui.ml
read _
sh runtest.sh tDraw.ml
read _
echo

echo "Fourth test set: Triangulation tests"
echo "Press [ENTER] to launch the last tests"
read _

echo
sh runtest.sh tTriangulation.ml
read _
echo

echo "-------------------- End of demo session --------------------"
