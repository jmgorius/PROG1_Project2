let draw_points ps =
  (** Draw a point set on screen *)
	let draw_point p =
		let ix = int_of_float (Point.Point.get_x p)
		and iy = int_of_float (Point.Point.get_y p) in
		Graphics.draw_circle ix iy 2
	in Point.PointSet.iter draw_point ps

let draw_triangles ts =
  (** Draw the given triangle set, using the Graphics module *)
	let draw_triangle t =
		let p1 = Triangle.Triangle.get_point t 1
		and p2 = Triangle.Triangle.get_point t 2
		and p3 = Triangle.Triangle.get_point t 3 in
		let l = [|(int_of_float (Point.Point.get_x p1),
			 int_of_float (Point.Point.get_y p1));
			 (int_of_float (Point.Point.get_x p2),
 			 int_of_float (Point.Point.get_y p2));
			 (int_of_float (Point.Point.get_x p3),
 			 int_of_float (Point.Point.get_y p3))|] in
		Graphics.draw_poly l
	in Triangle.TriangleSet.iter draw_triangle ts

let draw_edges es =
  (** Draw the given edges to the screen *)
  let edge_to_quadruple e =
    (** Convert an edge to a quadruple of coordinates in order to feed it into
        the Graphics module's drawing function *)
		let p1 = Edge.Edge.get_fst e
		and p2 = Edge.Edge.get_snd e in
		let x1 = Point.Point.get_x p1
		and y1 = Point.Point.get_y p1
		and x2 = Point.Point.get_x p2
		and y2 = Point.Point.get_y p2 in
		((int_of_float x1), (int_of_float y1),
		 (int_of_float x2), (int_of_float y2)) in
	let segment_list = List.map edge_to_quadruple (Edge.EdgeSet.elements es)
	in Graphics.draw_segments (Array.of_list segment_list)
