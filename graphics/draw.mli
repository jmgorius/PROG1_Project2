val draw_points: Point.point_set -> unit
val draw_triangles: Triangle.triangle_set -> unit
val draw_edges: Edge.edge_set -> unit
