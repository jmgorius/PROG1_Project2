let open_window width height =
	Graphics.close_graph ();
	let strw = string_of_int width
	and strh = string_of_int height in
	Graphics.open_graph (" " ^ strw ^ "x" ^ strh ^ "-0+0")

let get_width () = Graphics.size_x ()
let get_height () = Graphics.size_y ()

let clear_window col =
  (** Clear the window background to the given color *)
	let fg = Graphics.foreground in
	Graphics.set_color col;
	Graphics.fill_rect 0 0 (Graphics.size_x ()) (Graphics.size_y ());
	Graphics.set_color fg
