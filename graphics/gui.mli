val open_window: int -> int -> unit
val get_width: unit -> int
val get_height: unit -> int
val clear_window: Graphics.color -> unit
