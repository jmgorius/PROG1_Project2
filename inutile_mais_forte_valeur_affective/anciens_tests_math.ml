#use "tests.ml";;
#use "math.ml";;

open Point.Point;;
open Triangle.Triangle;;

print_endline "Math";;

let a = make_point 0.0 0.0;;
let b = make_point 0.0 1.0;;
let c = make_point 1.0 0.0;;
let d = make_point 2.0 0.0;;
let e = make_point 0.5 0.5;;

let abc = make_triangle a b c;;
let abd = make_triangle a b d;;

let l1 = TriangleSet.add abc (TriangleSet.empty());;
let l2 = TriangleSet.add abd (TriangleSet.empty());;

try
test (ccw a b c = true) "CCW 1";
test (ccw a b c = false) "CCW 2";
test (ccw b b b = true) "CCW 3";
test (ccw a c d = true) "CCW 4";

test (in_circle abc a = true) "In_circle 1";
test (in_circle abc e = true) "In_circle 2";
test (in_circle abc d = false) "In_circle 3";
test (in_circle abd e = true) "In_circle 4";

(* test border  *)

test (TriangleSet.is_equal (border l2) (border (add_point l2 e))) "Border 1";


with
  Fail -> print_newline() ; print_string "Tests stop : a test failed";;
