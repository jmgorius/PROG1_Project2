#use "vector.ml";;
  
module Angle :

sig
  val cosinus: Vector.vector -> Vector.vector -> float
  val sinus: Vector.vector -> Vector.vector -> float
  (* val angle: Vector.vector -> Vector.vector -> float *)
  
end

 =

 struct
   
   let cosinus vect_a vect_b =
     if Vector.is_null vect_a || Vector.is_null vect_b then raise Vector.VectorNull
     else (Vector.scal vect_a vect_b) /. (Vector.norm vect_a) /. (Vector.norm vect_b)


   let sinus vect_a vect_b =
     if Vector.is_null vect_a || Vector.is_null vect_b then raise Vector.VectorNull
     else (let a1 = Vector.get_a vect_a and
               a2 = Vector.get_b vect_a and
               b1 = Vector.get_a vect_b and
               b2 = Vector.get_b vect_b in
           let ax = (Point.get_x a1) -. (Point.get_x a2) and
               bx = (Point.get_x b1) -. (Point.get_x b2) and
               ay = (Point.get_y a1) -. (Point.get_y a2) and
               by = (Point.get_y b1) -. (Point.get_y b2) in
           ((Determinant.det2 [|[|ax; bx|]; [|ay; by|]|]) /. (Vector.norm vect_a)
            /. (Vector.norm vect_b)))
          

end;;
