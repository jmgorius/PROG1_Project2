let tri_edges t =
  (** Takes a triangles and returns a list of its edges*)
  let p1 = Triangle.Triangle.get_point t 1
  and p2 = Triangle.Triangle.get_point t 2
  and p3 = Triangle.Triangle.get_point t 3 in
  [Edge.Edge.make_edge p1 p2; Edge.Edge.make_edge p2 p3; Edge.Edge.make_edge p3 p1]

let edge_list tri_set = (** Takes a triangle set and returns a list of thes edges of the triangles*)
  let edge_list_l = List.map tri_edges (Triangle.TriangleSet.elements tri_set) in
  List.fold_left (@) [] ((edge_list_l) : Edge.edge list list)
  (* /!\ Some edges might occur twice in the list *)

let is_in_pair e l =
  (** Ascertains whether or not the edge e occurs (at least) twice in the list l *)
  let rec aux u n =
    match u with
    | [] -> n
    | x::xs -> if Edge.Edge.is_equal x e
               then begin
                   if n = 0 then aux xs 1
                   else 2
                 end
               else
                 aux xs n
  in
  ((aux l 0) = 2)

let rec delete e l =
  (** Delete the first occurence of the edge e in l *)
  match l with
  | [] -> failwith "Border.delete"
  | x::xs -> if Edge.Edge.is_equal e x then xs
             else x::(delete e xs)


let find_next e l =
  (** If e = (a,b), get an edge with starting vertex b in l *)
  (* Note: l has to contain edges forming a closed polygon *)
  let a = Edge.Edge.get_snd e in
  let rec aux u =
    (* u is never empty, as aux always finds and edge connected to e *)
    let x = List.hd u and
        xs = List.tl u in
    if (Edge.Edge.is_vertex x a) && (not (Edge.Edge.is_equal x e))
    then begin
        if Point.Point.is_equal (Edge.Edge.get_fst x) a
	then x
        else Edge.Edge.reverse x
      end
    else
      aux xs
  in
  aux l


let border_order edge_list =
  (** Orders the edge list so that its elements form a reverse chain,
i.e. every element has its first point equal to the last point of its successor *)
  let first_edge = List.hd edge_list in
  let rec aux u e=
    let next_edge = find_next e edge_list in
    if Edge.Edge.is_equal next_edge first_edge then u
    else aux (next_edge::u) next_edge
  in
  aux [first_edge] first_edge

let border tri_set = (** Returns the convex hull of the triangle set *)
  let edge_l = edge_list tri_set in
  let rec aux u =
    match u with
    | [] -> []
    | e::es -> if is_in_pair e u
               then aux (delete e es)
               else e::(aux es)
  in
  border_order (aux edge_l)

(*-----------------------------------------------------------------------------*)

let is_in_border e points =
  (** Ascertains whether or not the edge e is part of the convex hull of points *)
  let a = Edge.Edge.get_fst e in
  let in_right_side point =
    Edge.Edge.scal e (Edge.Edge.orthogonal (Edge.Edge.make_edge a point))  >= 0.
  in
  let right_side_set = List.map in_right_side (Point.PointSet.elements points) in
  List.fold_left (&&) true right_side_set

exception Next_point of Point.point

let find_next_point points last_point =
  (** Returns the next point p that is part of the convex hull of points.
   The edge (last_point, p) is an edge of the convex hull *)
  let aux point =
    if is_in_border (Edge.Edge.make_edge last_point point) points
    then raise (Next_point point)
    else ()
  in
  try
    Point.PointSet.iter aux points;
    last_point
      (* For type consistency, never reached *)
  with
  | Next_point point -> point

let convex_hull point_set =
  (** Returns the convex hull of the point set *)
  let first_point = Point.PointSet.min_elt point_set in
  let rec aux border_edge_list last_point =
    let point = find_next_point point_set last_point in
    if Point.Point.is_equal point first_point
    then (Edge.Edge.make_edge last_point point)::border_edge_list
    else aux ((Edge.Edge.make_edge last_point point)::border_edge_list) point
  in
  aux [] first_point
