val tri_edges: Triangle.triangle -> Edge.edge list
val edge_list: Triangle.triangle_set -> Edge.edge list
val is_in_pair: Edge.edge -> Edge.edge list -> bool
val find_next: Edge.edge -> Edge.edge list -> Edge.edge
val border_order: Edge.edge list -> Edge.edge list
val border: Triangle.triangle_set -> Edge.edge list

val is_in_border: Edge.edge -> Point.point_set -> bool
val find_next_point: Point.point_set -> Point.point -> Point.point
val convex_hull: Point.point_set -> Edge.edge list
