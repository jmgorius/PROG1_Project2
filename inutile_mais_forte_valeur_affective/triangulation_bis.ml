let init_tri_set point_set =
  (** Returns the initial triangle set, using the convex hull of the point set *)
  let conv_hull = Border_bis.convex_hull point_set
  and first_point = Point.PointSet.min_elt point_set in
  let rec aux u =
    match u with
    | [] -> Triangle.TriangleSet.empty
    | x::xs -> Triangle.TriangleSet.add (AddPoint.add_tri first_point x) (aux xs)
  in
  aux conv_hull

let interior point triangles =
  (** Returns true iff there exists a triangle having point inside its circumscribed circle *)
  Triangle.TriangleSet.exists (Utility.swap Circle.in_circle point) triangles

let delaunay points max_x max_y =
  (** Returns the Delaunay triangulation of points, given the dimensions of the window.
   The corners of the drawing area are included in the triangulation process. *)
  let tri_set = init_tri_set points in
  let rec traversal pts triangles =
    	match pts with
	| [] -> triangles
	| x::xs -> if interior x triangles then
			traversal xs (AddPoint.add_point triangles x)
		   else traversal xs triangles
  in
  traversal (Point.PointSet.elements points) tri_set

let delaunay_step_by_step points =
  (** Applies the triangulation algorithm, pausing between each point addition *)
  let tri_set = init_tri_set points in
  let rec traversal pts triangles =
      match pts with
      | [] -> ()
      | x::xs -> if interior x triangles then
      		      let new_set = AddPoint.add_point triangles x in
		      (Graphics.clear_graph (); Draw.draw_triangles new_set;
                      Draw.draw_points points);
		      ignore (Graphics.read_key ());
		      traversal xs (AddPoint.add_point triangles x)
		 else traversal xs triangles
  in
  traversal (Point.PointSet.elements points) tri_set
