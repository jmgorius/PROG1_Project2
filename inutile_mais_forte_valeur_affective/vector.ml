#use "det.ml";;

module Vector:

sig
  type vector
  exception VectorNull
  val create_vector: Point.point -> Point.point -> vector
  val is_null: vector -> bool
  val get_a: vector -> Point.point
  val get_b: vector -> Point.point
  val norm: vector -> float
  val scal: vector -> vector -> float 
end

  =

  struct
    type vector = {a: Point.point; b: Point.point}

    exception VectorNull
                
    let create_vector origin arrow =
      {a = origin; b = arrow}

    let is_null vect = (Point.is_equal vect.a vect.b)
                 
    let get_a vect = vect.a
    let get_b vect = vect.b

    let norm vect =
      let a = get_a vect and
          b = get_b vect in
      let ax = Point.get_x a and
          ay = Point.get_y a and
          bx = Point.get_x b and
          by = Point.get_y b in
      sqrt((bx-.ax)**2. +. (by-.ay)**2.)

    let scal vect_a vect_b =
      let a1 = vect_a.a and
          a2 = vect_a.b and
          b1 = vect_b.a and
          b2 = vect_b.b in
      let a1x = Point.get_x a1 and
          a1y = Point.get_y a1 and
          a2x = Point.get_x a2 and
          a2y = Point.get_y a2 and
          b1x = Point.get_x b1 and
          b1y = Point.get_y b1 and
          b2x = Point.get_x b2 and
          b2y = Point.get_y b2 in
      (((a2x-.a1x)*.(b2x-.b1x)) +. ((a2y-.a1y)*.(b2y-.b1y)))
  end;;
  
