let width = 800
and height = 600;;

Gui.open_window width height;;

let ps = RandomPoints.random 250 width height;;
Draw.draw_points (ps);;
Draw.draw_triangles (Triangulation.delaunay ps width height);;
ignore (Graphics.read_key ());;
