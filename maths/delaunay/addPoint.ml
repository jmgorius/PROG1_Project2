let find_tri tri_set point =
        (** Returns the set of triangles for which point is in the associated circumscribed circle *)
        Triangle.TriangleSet.filter (Utility.swap Circle.in_circle point) tri_set

let add_tri point e =
        (** Returns the triangle that has e for base and point for apex *)
        let a = Edge.Edge.get_fst e
        and b = Edge.Edge.get_snd e in
        Triangle.Triangle.make_triangle a b point

let add_point tri_set point = (** Add the point to the current triangulation *)
  (* Get the list of the triangles to update *)
  let to_change = find_tri tri_set point in
  (* Compute the associated border *)
  let border = Border.border to_change in
  (* Remove the elements of to_change from the triangle set *)
  let new_set = Triangle.TriangleSet.fold (Triangle.TriangleSet.remove)
                                          to_change tri_set in
  (* Compute the triangles to add using point *)
  let tri_to_add = Triangle.TriangleSet.of_list
                     (List.map (add_tri point) border) in
  (* Add the computed triangles to the unchanged ones *)
  Triangle.TriangleSet.fold Triangle.TriangleSet.add new_set tri_to_add
