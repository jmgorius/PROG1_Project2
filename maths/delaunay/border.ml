let tri_edges t =
  (** Takes a triangle and returns the list of its edges *)
  let p1 = Triangle.Triangle.get_point t 1
  and p2 = Triangle.Triangle.get_point t 2
  and p3 = Triangle.Triangle.get_point t 3 in
  [Edge.Edge.make_edge p1 p2; Edge.Edge.make_edge p2 p3; Edge.Edge.make_edge p3 p1]

let edge_list tri_set =
  (** Takes a triangle set and returns the list of thes edges of the triangles *)
  let edge_list_l = List.map tri_edges (Triangle.TriangleSet.elements tri_set)
  in List.fold_left (@) [] edge_list_l
  (* /!\ Some edges might occur twice in the list *)

let is_in_pair e l =
  (** Ascertains whether or not the edge e occurs (at least) twice in the list l *)
  let rec aux u n =
    match u with
    | [] -> n
    | x::xs -> if Edge.Edge.is_equal x e
               then begin
                   if n = 0 then aux xs 1
                   else 2
                 end
               else
                 aux xs n
  in
  ((aux l 0) = 2)

let rec delete e l =
  (** Delete the first occurence of the edge e in l *)
  match l with
  | [] -> failwith "Border.delete"
  | x::xs -> if Edge.Edge.is_equal e x then xs
             else x::(delete e xs)

let find_next e l =
  (** If e = (a,b), get an edge with starting vertex b in l *)
  (* Note: l has to contain edges forming a closed polygon *)
  let a = Edge.Edge.get_snd e in
  let rec aux u =
    (* u is never empty, as aux always finds and edge connected to e *)
    let x = List.hd u and
        xs = List.tl u in
    if (Edge.Edge.is_vertex x a) && (not (Edge.Edge.is_equal x e))
    then begin
        if Point.Point.is_equal (Edge.Edge.get_fst x) a
	then x
        else Edge.Edge.reverse x
      end
    else
      aux xs
  in
  aux l

let border_order edge_list =
  (** Orders the edge list so that its elements form a reverse chain, 
i.e. every element has its first point equal to the last point of its successor *)
  let first_edge = List.hd edge_list in
  let rec aux u e=
    let next_edge = find_next e edge_list in
    if Edge.Edge.is_equal next_edge first_edge then u
    else aux (next_edge::u) next_edge
  in
  aux [first_edge] first_edge

let border tri_set = (** Returns the convex hull of the triangle set *)
  let edge_l = edge_list tri_set in
  let rec aux u =
    match u with
    | [] -> []
    | e::es -> if is_in_pair e u
               then aux (delete e es)
               else e::(aux es)
  in
  border_order (aux edge_l)
