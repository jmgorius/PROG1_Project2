let init_tri_set max_x max_y =
  (** Returns the initial triangle set, given the dimensions of the window *)
  let p11 = Point.Point.make_point 0. 0.
  and p12 = Point.Point.make_point 0. (float_of_int max_y)
  and p13 = Point.Point.make_point (float_of_int max_x) 0. in
  let tri1 = Triangle.Triangle.make_triangle p11 p12 p13
  and p21 = Point.Point.make_point 0. (float_of_int max_y)
  and p22 = Point.Point.make_point (float_of_int max_x) (float_of_int max_y)
  and p23 = Point.Point.make_point (float_of_int max_x) 0. in
  let tri2 = Triangle.Triangle.make_triangle p21 p22 p23
  in
  Triangle.TriangleSet.add tri1 (Triangle.TriangleSet.add tri2 (Triangle.TriangleSet.empty))

let interior point triangles =
  (** Returns true iff there exists a triangle having point inside its circumscribed circle *) 
  Triangle.TriangleSet.exists (Utility.swap Circle.in_circle point) triangles

let delaunay points max_x max_y =
  (** Returns the Delaunay triangulation of points, given the dimensions of the window.
   The corners of the drawing area are included in the triangulation process. *)
  let tri_set = init_tri_set max_x max_y in
  let rec traversal pts triangles =
    	match pts with
	| [] -> triangles
	| x::xs -> if interior x triangles then
			traversal xs (AddPoint.add_point triangles x)
		   else traversal xs triangles
    in traversal (Point.PointSet.elements points) tri_set

let delaunay_step_by_step points =
  (** Applies the triangulation algorithm, pausing between each point addition *)
  let tri_set = init_tri_set (Gui.get_width ()) (Gui.get_height ()) in
  let rec traversal pts triangles =
      match pts with
      | [] -> ()
      | x::xs -> if interior x triangles then
      		      let new_set = AddPoint.add_point triangles x in
		      (Graphics.clear_graph (); Draw.draw_triangles new_set;
                      Draw.draw_points points);
		      ignore (Graphics.read_key ());
		      traversal xs (AddPoint.add_point triangles x)
		 else traversal xs triangles
  in traversal (Point.PointSet.elements points) tri_set
