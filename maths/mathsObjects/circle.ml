let ccw a b c =
        (** Ascertains whether or not the points a, b, c are in counter clockwise order *)
	let ax = Point.Point.get_x a
	and ay = Point.Point.get_y a
	and bx = Point.Point.get_x b
	and by = Point.Point.get_y b
	and cx = Point.Point.get_x c
	and cy = Point.Point.get_y c in
	(Det.det [|[|(bx-.ax);(cx-.ax)|]; [|(by-.ay);(cy-.ay)|]|]) >= 0.

let in_circle t pt =
        (** Ascertains whether or not pt is in the circumscribed circle of the triangle t *)
	let a = Triangle.Triangle.get_point t 1
	and b = Triangle.Triangle.get_point t 2
	and c = Triangle.Triangle.get_point t 3 in
	let ax = Point.Point.get_x a
	and ay = Point.Point.get_y a
	and bx = Point.Point.get_x b
	and by = Point.Point.get_y b
	and cx = Point.Point.get_x c
	and cy = Point.Point.get_y c
	and ptx = Point.Point.get_x pt
	and pty = Point.Point.get_y pt in
	if (ccw a b c) then
	((Det.det [|[|ax; ay; (ax*.ax +. ay*.ay); 1.|];
			     [|bx; by; (bx*.bx +. by*.by); 1.|];
			     [|cx; cy; (cx*.cx +. cy*.cy); 1.|];
			     [|ptx; pty; (ptx*.ptx +. pty*.pty); 1.|]|]) > 0.)
	else
	((Det.det [|[|ax; ay; (ax*.ax +. ay*.ay); 1.|];
			     [|bx; by; (bx*.bx +. by*.by); 1.|];
			     [|cx; cy; (cx*.cx +. cy*.cy); 1.|];
			     [|ptx; pty; (ptx*.ptx +. pty*.pty); 1.|]|]) < 0.)

        (* Those inequalities are strict: a point of the circumscribed circle won't be considered as inside it *)
