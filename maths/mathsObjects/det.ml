let det2 m = (** Returns the determinant of a 2x2 matrix of floats*)
	match m with
	| [|[|a1; a2|]; [|b1; b2|]|] -> (a1*.b2 -. a2*.b1)
	| _ -> failwith "The matrix m has the wrong format"


let det3 m = (** Returns the determinant of a 3x3 matrix of floats*)
	match m with
	| [|[|a1;a2;a3|]; [|b1;b2;b3|]; [|c1;c2;c3|]|] ->
	  ((a1*.b2*.c3) +. (a2*.b3*.c1) +. (a3*.b1*.c2)
	   -. (a1*.b3*.c2) -. (a2*.b1*.c3) -. (a3*.b2*.c1))
        (* this is the Sarrus method *)
	| _ -> failwith "The matrix m has the wrong format"

let det4 m = (** Returns the determinant of a 4x4 matrix of floats*)
	match m with
	| [|[|a1;a2;a3;a4|]; [|b1;b2;b3;b4|]; [|c1;c2;c3;c4|];
	  [|d1;d2;d3;d4|]|] ->
	  begin
	  let fst_cofactor = a1 *. det3 [|[|b2;b3;b4|]; [|c2;c3;c4|];
	   	[|d2;d3;d4|]|]
	  and scd_cofactor = b1 *. det3 [|[|a2;a3;a4|]; [|c2;c3;c4|];
	  	[|d2;d3;d4|]|]
	  and thd_cofactor = c1 *. det3 [|[|a2;a3;a4|]; [|b2;b3;b4|];
	  	[|d2;d3;d4|]|]
	  and fth_cofactor = d1 *. det3 [|[|a2;a3;a4|]; [|b2;b3;b4|];
	  	[|c2;c3;c4|]|] in
	  (fst_cofactor -. scd_cofactor +. thd_cofactor -. fth_cofactor)
	  end
        (* This is a Gaussian elimination using the 3x3 determinant *) 
	| _ -> failwith "The matrix m has the wrong format"

let det m =
  let n = Array.length m in
  if n = 2 then det2 m
  else if n = 3 then det3 m
  else det4 m
