module Edge =
	struct
		type t = Point.point * Point.point

		let compare e1 e2 =
			let (p11, p12) = e1
			and (p21, p22) = e2 in
			let c = Point.Point.compare p11 p21 in
			if c = 0 then Point.Point.compare p12 p22
			else c

		let get_fst ((a,b) : t) = a
		let get_snd ((a,b) : t) = b

		let make_edge a b =
			((a,b): t)

		let norm e = (** Returns the length of an edge *)
			let p1 = get_fst e
			and p2 = get_snd e in
			let x1 = Point.Point.get_x p1
			and y1 = Point.Point.get_y p1
			and x2 = Point.Point.get_x p2
			and y2 = Point.Point.get_y p2 in
			sqrt ((x2 -. x1)**2. +. (y2 -. y1)**2.)

    let orthogonal e =
      let a = get_fst e
      and b = get_snd e in
      let ax = Point.Point.get_x a
      and ay = Point.Point.get_y a
      and bx = Point.Point.get_x b
      and by = Point.Point.get_y b in
      let deltax = bx -. ax
      and deltay = by -. ay in
      let new_b = Point.Point.make_point (ax+.deltax) (ay+.deltay) in
      make_edge a new_b

		let scal e1 e2 = (** Returns the dot product of two edges, considered as vectors *)
		  (* /!\ the edges are here considered as oriented*)
		  	let e1_1 = get_fst e1
			and e1_2 = get_snd e1
			and e2_1 = get_fst e2
			and e2_2 = get_snd e2 in
			let e1_1x = Point.Point.get_x e1_1
			and e1_1y = Point.Point.get_y e1_1
			and e1_2x = Point.Point.get_x e1_2
			and e1_2y = Point.Point.get_y e1_2
			and e2_1x = Point.Point.get_x e2_1
			and e2_1y = Point.Point.get_y e2_1
			and e2_2x = Point.Point.get_x e2_2
			and e2_2y = Point.Point.get_y e2_2 in
			(((e1_2x-.e1_1x)*.(e2_2x-.e2_1x)) +.
			 ((e1_2y-.e1_1y)*.(e2_2y-.e2_1y)))

		let reverse e =
			make_edge (get_snd e) (get_fst e)

		let is_equal edge1 edge2 =
			((Point.Point.is_equal (get_fst edge1)
			 (get_fst edge2)) &&
			 (Point.Point.is_equal (get_snd edge1)
			  (get_snd edge2))) ||
			  ((Point.Point.is_equal (get_fst edge1)
			   (get_snd edge2)) &&
			   (Point.Point.is_equal (get_snd edge1)
			    (get_fst edge2)))

		let is_vertex e point = (** Ascertains whether or not
			 the point point is a vertex of the edge e*)
			(Point.Point.is_equal point (get_fst e)) ||
			 (Point.Point.is_equal point (get_snd e))
end

module EdgeSet = Set.Make(Edge)

type edge = Edge.t
type edge_set = EdgeSet.t
