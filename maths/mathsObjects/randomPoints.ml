(** Initialize random number generator *)
Random.self_init ()

let rand_point max_x max_y = (** Returns a random point in the rectangle  (0.,0.) (0.,max_y) (max_x, max_y) (max_x, 0.)*)
	let x = Random.float (float_of_int max_x)
	and y = Random.float (float_of_int max_y) in
	Point.Point.make_point x y

let random nb max_x max_y = (** Returns a random point set of nb points in the rectangle  (0.,0.) (0.,max_y) (max_x, max_y) (max_x, 0.)*)
	let rec random_aux n =
		if n = 0 then Point.PointSet.empty
		else
		Point.PointSet.add (rand_point max_x max_y) (random_aux (n-1))
	in random_aux nb
