#!/bin/sh

rlwrap ocaml -I core -I maths/delaunay -I maths/mathsObjects -I tests -I graphics unix.cma graphics.cma core/point.cmo core/triangle.cmo core/utility.cmo maths/mathsObjects/edge.cmo maths/mathsObjects/det.cmo maths/mathsObjects/randomPoints.cmo maths/mathsObjects/circle.cmo  maths/delaunay/border.cmo maths/delaunay/addPoint.cmo graphics/draw.cmo graphics/gui.cmo maths/delaunay/triangulation.cmo $1
