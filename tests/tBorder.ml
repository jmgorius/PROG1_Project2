open Tests;;
open Border;;
open Edge;;
open Gui;;
open Draw;;

print_endline "Border";;

let a = Point.Point.make_point 250.0 200.0;;
let c = Point.Point.make_point 200.0 425.0;;
let b = Point.Point.make_point 400.0 200.0;;
let d = Point.Point.make_point 350.0 450.0;;
let e = Point.Point.make_point 5. 5.;;

let abc = Triangle.Triangle.make_triangle a b c;;
let acd = Triangle.Triangle.make_triangle c d b;;

let ab = Edge.make_edge a b;;
let ba = Edge.make_edge b a;;
let cd = Edge.make_edge c d;;
let bc = Edge.make_edge b c;;

let l1 = Triangle.TriangleSet.add abc (Triangle.TriangleSet.empty);;
let l2 = Triangle.TriangleSet.add acd (Triangle.TriangleSet.empty);;
let l3 = Triangle.TriangleSet.add acd l1;;

let es1 = [ab];;
let es2 = [ab;ab];;
let es3 = [ab;ba];;
let es4 = [ab;cd];;
let es5 = [ab;cd;ba;bc];;

let print_edge e =
	let p1 = Edge.get_fst e
	and p2 = Edge.get_snd e in
	let x1 = Point.Point.get_x p1
	and y1 = Point.Point.get_y p1
	and x2 = Point.Point.get_x p2
	and y2 = Point.Point.get_y p2 in
	print_float x1; print_string ", "; print_float x2; print_string ", ";
	print_float y1; print_string ", "; print_float y2; print_string " || ";;

try
        test (is_in_pair ab es1 = false) "is_in_pair 1";
        test (is_in_pair ab es2 = true) "is_in_pair 2";
        test (is_in_pair ab es3 = true) "is_in_pair 3";
        test (is_in_pair ab es4 = false) "is_in_pair 4";
        test (is_in_pair ab es5 = true) "is_in_pair 5";
        let el = Border_bis.edge_list l3 in
        test(is_in_pair bc el = true) "Is_in_pair 6";

        test (find_next ab es5 = bc) "find_next";

        test (List.mem ab (tri_edges abc) = true) "tri_edges 1";
        test (List.mem cd (tri_edges abc) = false) "tri_edges 2";


with
  Fail -> print_newline() ; print_endline "Tests stop : a test failed";;


open_window 800 600;;

draw_triangles l3;;
ignore (Graphics.read_key ());;
clear_window (Graphics.rgb 255 255 255);;

let b = EdgeSet.of_list (border l3);;

draw_edges (b);;
ignore (Graphics.read_key ());;
