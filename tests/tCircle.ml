open Tests;;

open Point.Point;;
open Triangle.Triangle;;
open Circle;;

print_endline "Circle";;

let a = make_point 0.0 0.0;;
let c = make_point 0.0 1.0;;
let b = make_point 1.0 0.0;;
let d = make_point 2.0 0.0;;
let e = make_point 0.5 0.5;;

let abc = make_triangle a b c;;
let acd = make_triangle a c d;;

let l1 = Triangle.TriangleSet.add abc (Triangle.TriangleSet.empty);;
let l2 = Triangle.TriangleSet.add acd (Triangle.TriangleSet.empty);;

try
        test (ccw a b c = true) "CCW 1";
        test (ccw a c b = false) "CCW 2";
        test (ccw b b b = true) "CCW 3";
        test (ccw a b d = true) "CCW 4";

        test (in_circle abc a = false) "In_circle 1";
        test (in_circle abc e = true) "In_circle 2";
        test (in_circle abc d = false) "In_circle 3";
        test (in_circle acd e = true) "In_circle 4";

with
  Fail -> print_newline() ; print_endline "Tests stop : a test failed";;
