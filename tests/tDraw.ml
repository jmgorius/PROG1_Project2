open Tests;;

open Draw;;

print_endline "Draw";;

let a = Point.Point.make_point 400. 100.;;
let b = Point.Point.make_point 200. 500.;;
let c = Point.Point.make_point 710. 300.;;

let t = Triangle.Triangle.make_triangle a b c;;

let ts = Triangle.TriangleSet.add t (Triangle.TriangleSet.empty);;

Gui.open_window 800 600;;
draw_points (Point.PointSet.add (Point.Point.make_point 400. 300.) (Point.PointSet.empty));;
ignore (Graphics.read_key ());;

draw_triangles ts;;
ignore (Graphics.read_key ());;
