open Tests;;
open Point.Point;;
open Edge.Edge;;

print_endline "Edge";;

let a = make_point 0.0 0.0;;
let b = make_point 1.0 1.0;;
let c = make_point 1.0 0.0;;
let d = make_point 0.0 1.0;;

let ab = make_edge a b;;
let bc = make_edge b c;;
let cd = make_edge c d;;
let dc = make_edge d c;;

try
        test (get_fst ab = a) "Get_fst";
        test (get_snd ab = b) "get_snd";

        test (is_equal ab ab = true) "Is_equal 1";
        test (is_equal cd dc = true) "Is_equal 2";
        test (is_equal ab bc = false) "Is_equal 3";

        test (get_fst (reverse cd) = d) "Reverse 1";
        test (get_snd (reverse cd) = c) "Reverse 2";

        test (is_vertex ab a = true) "Is_next 1";
        test (is_vertex ab b = true) "Is_next 2";
        test (is_vertex ab c = false) "Is_next 3";
with
  Fail -> print_newline() ; print_endline "Tests stop : a test failed";;
