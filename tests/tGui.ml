open Tests;;
open Gui;;

print_endline "GUI";;


open_window 800 600;;

print_string "Size of the window :";;
print_int (get_width ());;
print_string "x";;
print_int (get_height ());;
print_string " (Pass if 800x600)";;
print_newline ();;

ignore (Graphics.read_key ());;

clear_window (Graphics.rgb 43 72 128);;
ignore (Graphics.read_key ());;
