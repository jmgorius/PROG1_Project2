open Tests;;
open Point.Point;;

print_endline "Point";;

let p = make_point 0.0 1.0;;
let q = make_point 1.0 0.0;;
let r = make_point 1.0 1.0;;
let s = make_point 1.0 0.0;;

try
        print_endline "- No test for make_point";

        test (get_x p = 0.0) "Get x";
        test (get_y p = 1.0) "Get y";

        test (compare p p = 0) "Is_equal 1";
        test (is_equal p q = false) "Is_equal 2";
        test (is_equal p r = false) "Is_equal 3";
        test (is_equal p s = false) "Is_equal 4";

with
  Fail -> print_newline() ; print_endline "Tests stop : a test failed";;
