open Tests;;
open Point.Point;;
open Triangle.Triangle;;

print_endline "Triangle";;

let a = make_point 0.0 0.0 ;;
let b = make_point 0.0 1.0;;
let c = make_point 1.0 0.0;;

let t = make_triangle a b c;;
let q = make_triangle a c b;;
let r = make_triangle c a b;;

try
        print_endline "- No test for make_triangle";

        test (get_point t 1 = a) "Get_point 1";
        test (get_point t 2 = b) "Get_point 2";
        test (get_point t 3 = c) "Get_point 3";
with
        Fail -> print_newline() ; print_endline "Tests stop : a test failed";;
