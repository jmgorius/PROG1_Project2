open Tests;;
open Border;;
open Gui;;
open Draw;;
open Triangulation;;

print_endline "Triangulation";;

let w=800 and h=600;;

let ps = RandomPoints.random 5 w h;;
open_window w h;;
draw_points (ps);;
ignore (Graphics.read_key ());;
delaunay_step_by_step ps;;

Graphics.clear_graph ();;
let ps = RandomPoints.random 1000 w h;;
draw_points (ps);;
draw_triangles (delaunay ps w h);;
ignore (Graphics.read_key ());;
