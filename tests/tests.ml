exception Fail;;

print_string "Tests of ";;

let test t str =
  (** Validate or invalidate the test t *)
  print_string "- ";
  print_string str;
  if t
    then (print_endline " pass" )
  else (print_endline " fail"; raise Fail )
;;
